# rendfetch

## Installation
`npm i rendfetch`

## Description
Request a webpage and return the rendered HTML after evaluation of the JavaScript.

## Usage
```
const rendfetch = require('rendfetch');

rendfetch("https://allegiancefinance.herokuapp.com").then(html => {
  console.log(html);
});
```
