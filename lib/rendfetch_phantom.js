/* rendfetch.js - Request a JS rendered webpage via phantomjs. */
/* TODO add debug output. */

const phantom = require('phantom');
 
async function rendfetch(url) {
  const instance = await phantom.create();
  const page = await instance.createPage();
  const status = await page.open(url);
  const content = await page.property('content');
  // const headers = await page.property('headers');
  await instance.exit();
  return content;
}

module.exports = rendfetch;
