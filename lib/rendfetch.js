/** rendfetch.js - Fetch a webpage and render any JS to HTML.
 *                 Utilizes Zombie.js.
 */

const Browser = require('zombie');

/*
function rendfetch_new(url, callb) {
  let result = {};
  browser = new Browser();
  browser.fetch(url)
  .then(function(response) {
    result.headers = response.headers;
  })
  .then(function(html) {
    result.html = html;
    callb(result);
  })
  .catch(()=> undefined);
}
*/

function rendfetch(url, callb) {
  browser = new Browser();
  browser.visit(url, function() {
    let html = browser.html(); 
    if(!html) return callb({html: '', headers: null});
    let headers = browser.resources['0'].response.headers;
    browser.tabs.closeAll();
    browser.destroy();
    return callb({ html, headers });
  });
}

module.exports = rendfetch;
